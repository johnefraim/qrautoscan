package com.eh.phjosa.qrscanner;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;

public class MainActivity extends AppCompatActivity {
    Button scanBtn;
    public static final int REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scanBtn = findViewById(R.id.btn_scan);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
        }
        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ScanActivity.class);
                startActivityForResult(i, REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                Barcode barcode = data.getParcelableExtra("barcode");
                Toast.makeText(this, "RESULT: " + barcode.displayValue, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "error!!! ", Toast.LENGTH_LONG).show();
            }
//            if (data != null) {
////                Frame frame = new Frame.Builder().setBitmap(bitmap).build();
////                Barcode barcode = data.getParcelableExtra("barcode");
////                SparseArray<Barcode> barcodes = detector.detect(frame);
////                for (int index = 0; index < barcodes.size(); index++) {
////                    Barcode code = barcodes.valueAt(index);
////                    scanResults.setText(scanResults.getText() + code.displayValue + "\n");
////
////                    //Required only if you need to extract the type of barcode
////                    int type = barcodes.valueAt(index).valueFormat;
////                    switch (type) {
////                        case Barcode.CONTACT_INFO:
////                            Log.i(LOG_TAG, code.contactInfo.title);
////                            break;
////                        case Barcode.EMAIL:
////                            Log.i(LOG_TAG, code.email.address);
////                            break;
////                        case Barcode.ISBN:
////                            Log.i(LOG_TAG, code.rawValue);
////                            break;
////                        case Barcode.PHONE:
////                            Log.i(LOG_TAG, code.phone.number);
////                            break;
////                        case Barcode.PRODUCT:
////                            Log.i(LOG_TAG, code.rawValue);
////                            break;
////                        case Barcode.SMS:
////                            Log.i(LOG_TAG, code.sms.message);
////                            break;
////                        case Barcode.TEXT:
////                            Log.i(LOG_TAG, code.rawValue);
////                            break;
////                        case Barcode.URL:
////                            Log.i(LOG_TAG, "url: " + code.url.url);
////                            break;
////                        case Barcode.WIFI:
////                            Log.i(LOG_TAG, code.wifi.ssid);
////                            break;
////                        case Barcode.GEO:
////                            Log.i(LOG_TAG, code.geoPoint.lat + ":" + code.geoPoint.lng);
////                            break;
////                        case Barcode.CALENDAR_EVENT:
////                            Log.i(LOG_TAG, code.calendarEvent.description);
////                            break;
////                        case Barcode.DRIVER_LICENSE:
////                            Log.i(LOG_TAG, code.driverLicense.licenseNumber);
////                            break;
////                        default:
////                            Log.i(LOG_TAG, code.rawValue);
////                            break;
////                    }
////                }
////                if (barcodes.size() == 0) {
////                    scanResults.setText("Scan Failed: Found nothing to scan");
////                    Toast.makeText(this, "error!!! ", Toast.LENGTH_LONG).show();
////                }
////            } else {
////                scanResults.setText("Could not set up the detector!");
////            }
        }
    }
}
